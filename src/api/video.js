/*
* @Description: API详细列表
* @version: 仿抖音PC版
* @author: 追风少年
* 开源地址: https://gitee.com/lifeixue/short-video-pc
* 注意：请尊重开发者劳动成果,开源项目仅供参考学习之用,切勿用于其他的商业用途
*/
import axios from '@/http/http.js'

// 封装一系列 视频相关 的请求方法
const video = {
    // 热门视频
    hotList(data) {
        return axios.get('/hot', data)
    },
}

export default video