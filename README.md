# 开源项目：短视频播放,仿抖音PC版

## 项目介绍
短视频项目（仿抖音PC端页面）首页支持视频预览，播放页支持上下滑动、上下按钮、鼠标滚轮的方式来进行视频的切换操作。

## 执行命令
初始化：npm install  
运行：npm run serve  
编译: npm run build  

## 使用说明：
(1) 下载项目源码至本地,项目目录下执行 npm install 命令,来初始化项目  
(2) 执行 npm run serve 命令,来启动运行项目  
(3) 执行 npm run build 命令,可以对项目源码进行打包/发布（自动生成的 dist 文件夹,就是编译后的文件,可直接上传至服务器端来进行访问）

## 演示图例
![输入图片说明](https://gitee.com/lifeixue/short-video-pc/raw/master/src/assets/1.png)
![输入图片说明](https://gitee.com/lifeixue/short-video-pc/raw/master/src/assets/2.png)

## 使用注意事项
请尊重开发者劳动成果,开源项目仅供参考学习之用,切勿用于商业及其他用途!
